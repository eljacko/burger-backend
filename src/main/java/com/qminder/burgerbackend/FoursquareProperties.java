package com.qminder.burgerbackend;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@ToString
@Component
@ConfigurationProperties(prefix = "foursquare")
public class FoursquareProperties {
    private String clientId;
    private String clientSecret;
    private String query;
    private Boolean useProxy;
}
