package com.qminder.burgerbackend.api.dto.foursquare;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
public class Venue {
    private String name;
    private String id;
    private Location location;
    private List<Category> categories;
}
