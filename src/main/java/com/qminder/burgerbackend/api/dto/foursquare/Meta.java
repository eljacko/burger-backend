package com.qminder.burgerbackend.api.dto.foursquare;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Meta {
    private Integer code;
    private String requestId;
}
