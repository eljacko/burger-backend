package com.qminder.burgerbackend.api.dto.foursquare.explore;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.qminder.burgerbackend.api.dto.foursquare.Venue;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
public class Item {
    private Venue venue;
}
