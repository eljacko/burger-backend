package com.qminder.burgerbackend.api.dto.burger;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BurgerListResponse {
    private List<BurgerListItem> burgers;

    public BurgerListResponse(List<BurgerListItem> burgers) {
        this.burgers = burgers;
    }

    public final String toStringMainData() {
        StringBuilder builder = new StringBuilder();
        builder.append("BurgerListResponse, burgers size=");
        if (CollectionUtils.isEmpty(burgers)) {
            builder.append(0);
        } else {
            builder.append(burgers.size());
        }
        builder.append("]");
        return builder.toString();
    }
}
