package com.qminder.burgerbackend.api.dto.burgerApi;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class BurgerApiRequest {
    private List<String> urls;
}
