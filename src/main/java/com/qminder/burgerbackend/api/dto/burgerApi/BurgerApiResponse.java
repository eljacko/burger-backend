package com.qminder.burgerbackend.api.dto.burgerApi;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BurgerApiResponse {
    private String urlWithBurger;
}
