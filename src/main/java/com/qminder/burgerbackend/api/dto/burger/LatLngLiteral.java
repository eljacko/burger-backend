package com.qminder.burgerbackend.api.dto.burger;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class LatLngLiteral {
    private Double lat;
    private Double lng;
}
