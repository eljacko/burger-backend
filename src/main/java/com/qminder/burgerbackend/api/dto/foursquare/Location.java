package com.qminder.burgerbackend.api.dto.foursquare;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
public class Location {
    private Integer distance;
    private String address;
    private Double lat;
    private Double lng;
    private String city;
    private String state;
    private String country;
    private String postalCode;
}
