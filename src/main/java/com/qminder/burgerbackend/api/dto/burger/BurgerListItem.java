package com.qminder.burgerbackend.api.dto.burger;

import com.qminder.burgerbackend.api.dto.foursquare.Location;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BurgerListItem {
    private String name;
    private LatLngLiteral location;
    private String latestPhoto;

    public BurgerListItem (final String name, final String latestPhoto, final Location location) {
        this.name = name;
        this.latestPhoto = latestPhoto;
        this.location = new LatLngLiteral(location.getLat(), location.getLng());
    }
}
