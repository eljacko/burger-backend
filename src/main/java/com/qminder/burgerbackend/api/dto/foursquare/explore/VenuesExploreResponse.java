package com.qminder.burgerbackend.api.dto.foursquare.explore;

import com.qminder.burgerbackend.api.dto.foursquare.Meta;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class VenuesExploreResponse {
    private Response response;
    private Meta meta;
}
