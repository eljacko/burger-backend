package com.qminder.burgerbackend.api.service;

import com.qminder.burgerbackend.api.dto.foursquare.Venue;
import com.qminder.burgerbackend.api.dto.foursquare.explore.Item;
import com.qminder.burgerbackend.api.dto.foursquare.explore.VenuesExploreResponse;
import com.qminder.burgerbackend.api.exception.ExternalServiceException;
import com.qminder.burgerbackend.util.DateUtilService;
import com.qminder.burgerbackend.util.FoursquareUrlBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@Slf4j
@Getter
@Setter
@AllArgsConstructor
public class BurgerVenueExploreService {
    private final RestTemplate restTemplate;
    private final FoursquareUrlBuilder urlBuilder;
    private final DateUtilService dateUtilService;

    public List<Venue> getVenuesForLocation(){
        try {
            VenuesExploreResponse venuesExploreResponse = restTemplate.getForObject(
                    urlBuilder.buildSearchUrl(getDateUtilService().getDateAsVersion()),
                    VenuesExploreResponse.class);
            assert venuesExploreResponse != null;
            return venuesExploreResponse.getResponse().getGroups().stream()
                    .flatMap(group -> group.getItems().stream())
                    .map(Item::getVenue).collect(toList());
        } catch (Exception ex){
            log.warn("Error getting response from Foursquare", ex);
            throw new ExternalServiceException();
        }
    }
}
