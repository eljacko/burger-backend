package com.qminder.burgerbackend.api.service;

import com.qminder.burgerbackend.api.dto.burger.BurgerListResponse;

public interface BurgerListService {

    BurgerListResponse getBurgerList (Boolean async);

}
