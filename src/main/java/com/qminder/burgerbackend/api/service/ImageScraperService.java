package com.qminder.burgerbackend.api.service;

import com.qminder.burgerbackend.FoursquareProperties;
import com.qminder.burgerbackend.constant.Milliseconds;
import com.qminder.burgerbackend.util.FoursquareUrlBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
@CacheConfig(cacheNames = {"images_cache"})
public class ImageScraperService {

    @Cacheable(sync = true)
    public List<String> getImageLinks(final String urlString) throws IOException {
        log.debug("Scraping images for {}", urlString);
        List<String> imageLinks = new ArrayList<>();
        URL url = null;
        try {
            url = new URL(urlString);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Document document = Jsoup.parse(url, (int) (5 * Milliseconds.SECOND));
        Elements imageElements = document.select("img");
            for (Element el : imageElements) {
                String imageLink = el.absUrl("src");
                imageLinks.add(imageLink);
            }
        return imageLinks;
    }
}
