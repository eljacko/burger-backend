package com.qminder.burgerbackend.api.service;

import com.qminder.burgerbackend.AsyncConfiguration;
import com.qminder.burgerbackend.api.dto.burger.BurgerListItem;
import com.qminder.burgerbackend.api.dto.burger.BurgerListResponse;
import com.qminder.burgerbackend.api.dto.foursquare.Location;
import com.qminder.burgerbackend.api.dto.foursquare.Venue;
import com.qminder.burgerbackend.api.exception.ApplicationException;
import com.qminder.burgerbackend.util.FoursquareUrlBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
@Slf4j
@RequiredArgsConstructor
public class FuorsquareBurgerListService implements BurgerListService {

    private final BurgerVenueExploreService burgerVenueExploreService;
    private final ImageScraperService imageScraperService;
    private final BurgerPhotoValidationService burgerPhotoValidationService;
    private final AsyncConfiguration asyncConfiguration;
    private final FoursquareUrlBuilder urlBuilder;

    public BurgerListResponse getBurgerList (final Boolean async) {
        List<Venue> venues = burgerVenueExploreService.getVenuesForLocation();
        if (async) {
            return getResponseAsync(venues);
        } else {
            return getResponse(venues);
        }
    }

    private BurgerListResponse getResponse (final List<Venue> venues) {
        BurgerListResponse response = new BurgerListResponse(new ArrayList<>());
        for (Venue venue : venues) {
            BurgerListItem listItem = getBurgerListItem(venue.getName(),
                    venue.getId(), venue.getLocation());
            response.getBurgers().add(listItem);
        }
        return response;
    }

    private BurgerListResponse getResponseAsync (final List<Venue> venues) {
        List<BurgerListItem> burgerListItems = new ArrayList<>();
        final CompletableFuture[] itemsFuture = venues.stream()
                .map(venue -> CompletableFuture.runAsync(
                        () -> burgerListItems.add(getBurgerListItem(venue.getName(),
                                venue.getId(), venue.getLocation())),
                        asyncConfiguration.taskExecutor()))
                .toArray(CompletableFuture[]::new);

        final CompletableFuture<Void> allItems = CompletableFuture.allOf(itemsFuture);
        try {
            allItems.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new ApplicationException("Failed to compute burgers list");
        }
        return new BurgerListResponse(burgerListItems);
    }

    private BurgerListItem getBurgerListItem(final String name, final String id,
                                             final Location location) {
        List<String> photos = new ArrayList<>();
        try {
            photos = imageScraperService.getImageLinks(urlBuilder.buildVenuePhotosUrlString(name, id));
        } catch (IOException e) {
            log.debug("Failed to scrape images from fourscuare for:{}, id:{}", name, id);
        }
        String photo = photos.isEmpty() ? null
                : burgerPhotoValidationService.getPhotoWithBurger(photos);
        return new BurgerListItem(name, photo, location);
    }
}
