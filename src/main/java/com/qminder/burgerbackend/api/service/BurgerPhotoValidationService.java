package com.qminder.burgerbackend.api.service;

import com.qminder.burgerbackend.api.dto.burgerApi.BurgerApiRequest;
import com.qminder.burgerbackend.api.dto.burgerApi.BurgerApiResponse;
import com.qminder.burgerbackend.constant.ExternalServicesUrls;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@Slf4j
@Getter
@Setter
@AllArgsConstructor
public class BurgerPhotoValidationService {

    public String getPhotoWithBurger(final List<String> photos) {
        log.debug("List of photos to validate for burgers {}", photos);
        BurgerApiRequest burgerApiRequest = new BurgerApiRequest(photos);
        try {
            BurgerApiResponse burgerApiResponse = new RestTemplate()
                    .postForObject(ExternalServicesUrls.PHOTO_VALIDATION_URL,
                            new MappingJacksonValue(burgerApiRequest), BurgerApiResponse.class );
            assert burgerApiResponse != null;
            return burgerApiResponse.getUrlWithBurger();
        } catch (Exception ex){
            log.debug("Validating list for burgers failed {}", photos);
            return null;
        }
    }
}
