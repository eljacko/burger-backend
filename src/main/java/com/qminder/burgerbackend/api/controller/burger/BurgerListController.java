package com.qminder.burgerbackend.api.controller.burger;

import com.qminder.burgerbackend.api.dto.burger.BurgerListResponse;
import com.qminder.burgerbackend.api.service.FuorsquareBurgerListService;
import com.qminder.burgerbackend.constant.BurgerBackendApiPaths;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/v1")
@SuppressWarnings("checkstyle:designforextension")
public class BurgerListController {

    private final FuorsquareBurgerListService fuorsquareBurgerListService;

    @GetMapping(BurgerBackendApiPaths.BURGERS)
    public HttpEntity<?> getBurgers(
            @RequestParam(value = "async", defaultValue = "true") final Boolean async) {
        log.debug("Request to get burgers for Tartu");
        BurgerListResponse response = fuorsquareBurgerListService.getBurgerList(async);
        log.debug("Found Burger venues data {}", response.toStringMainData());

        return new ResponseEntity<>(new MappingJacksonValue(response), HttpStatus.OK);
    }
}
