package com.qminder.burgerbackend.api.exception;

import com.qminder.burgerbackend.api.dto.FieldError;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class InvalidParametersException extends RuntimeException {

    private static final long serialVersionUID = 7979151036772999042L;
    private List<FieldError> fieldErrors = null;

    public InvalidParametersException(final List<FieldError> fieldErrors) {
        super();
        this.fieldErrors = fieldErrors;
    }

}
