package com.qminder.burgerbackend.api.exception;

@SuppressWarnings({ "checkstyle:FinalParameters" })
public class ExternalServiceException extends RuntimeException {
    private static final long serialVersionUID = 4076119488371982779L;

    public ExternalServiceException() {
        super();
    }

    public ExternalServiceException(final String message) {
        super(message);
    }

    public ExternalServiceException(final String message, Throwable cause) {
        super(message, cause);
    }

    public ExternalServiceException(Throwable cause) {
        super(cause);
    }
}
