package com.qminder.burgerbackend.constant;

public final class ExternalServicesUrls {
    public static final String VENUES_BASE_URL = "https://api.foursquare.com/v2/venues/explore"
            + "?client_id=%s&client_secret=%s&ll=%s&query=%s&v=%s";
    public static final String VENUES_BASE_URL_TARTU = "https://api.foursquare.com/v2/venues/explore"
            + "?client_id=%s&client_secret=%s&near=Tartu&query=%s&v=%s";

    public static final String PHOTOS_BASE_URL = "https://foursquare.com/v/%s/%s/photos";
    public static final String PHOTO_VALIDATION_URL =
            "https://pplkdijj76.execute-api.eu-west-1.amazonaws.com/prod/recognize";

    private ExternalServicesUrls() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
