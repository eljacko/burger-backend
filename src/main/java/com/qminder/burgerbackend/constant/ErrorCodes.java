package com.qminder.burgerbackend.constant;

public final class ErrorCodes {

    public static final int INVALID_DATA = 11;

    public static final int INACTIVE_API_SERVICE = 70;

    public static final int UNKNOWN_EXCEPTION = 100;
    public static final int APPLICATION_EXCEPTION = 101;
    public static final int EXTERNAL_SERVICE_EXCEPTION = 102;

    private ErrorCodes() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
