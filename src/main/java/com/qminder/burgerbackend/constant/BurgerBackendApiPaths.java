package com.qminder.burgerbackend.constant;

public final class BurgerBackendApiPaths {

    public static final String BURGERS = "/burgers";

    private BurgerBackendApiPaths() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
