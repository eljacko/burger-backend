package com.qminder.burgerbackend.constant;

public final class Milliseconds {
    public static final long SECOND = 1000L;
    public static final long MINUTE = 60_000L;
    public static final int MINUTE_INT = 60_000;

    private Milliseconds() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
