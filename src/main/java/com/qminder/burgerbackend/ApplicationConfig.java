package com.qminder.burgerbackend;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

@Slf4j
@Configuration
@EnableAspectJAutoProxy
@EnableCaching
//@ComponentScan("com.qminder.burgerbackend.database")
@SuppressWarnings("checkstyle:designforextension")
public class ApplicationConfig {

    @Bean
    public GsonJsonParser gsonJsonParser() {
        return new GsonJsonParser();
    }

    @Bean
    public Gson gson() {
        return new Gson();
    }

    @Bean
    @SuppressWarnings({"checkstyle:FinalParameters", "checkstyle:MagicNumber"})
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        // @formatter:off
        return builder
            .setConnectTimeout(Duration.ofMinutes(3))
            .setReadTimeout(Duration.ofMinutes(5))
            .build();
        // @formatter:on
    }

    @Bean
    public Caffeine caffeineConfig() {
        return Caffeine.newBuilder().expireAfterWrite(60, TimeUnit.MINUTES);
    }

    @Bean
    public CacheManager cacheManager(Caffeine caffeine) {
        CaffeineCacheManager caffeineCacheManager = new CaffeineCacheManager();
        caffeineCacheManager.setCaffeine(caffeine);
        return caffeineCacheManager;
    }

}
