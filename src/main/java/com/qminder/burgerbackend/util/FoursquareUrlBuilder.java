package com.qminder.burgerbackend.util;

import com.qminder.burgerbackend.FoursquareProperties;
import com.qminder.burgerbackend.constant.ExternalServicesUrls;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;

@Component
@Getter
@Setter
public class FoursquareUrlBuilder {
    private FoursquareProperties properties;

    @Autowired
    FoursquareUrlBuilder(FoursquareProperties properties) {
        this.properties = properties;
    }

    public String buildSearchUrl(final String location, final String version) {
        return String.format(ExternalServicesUrls.VENUES_BASE_URL,
                properties.getClientId(),
                properties.getClientSecret(),
                location,
                properties.getQuery(),
                version
        );
    }

    public String buildSearchUrl(final String version) {
        return String.format(ExternalServicesUrls.VENUES_BASE_URL_TARTU,
                properties.getClientId(),
                properties.getClientSecret(),
                properties.getQuery(),
                version
        );
    }

    public String buildVenuePhotosUrlString(final String name, final String id) {
        String formattedName = name.toLowerCase()
                .replace(' ', '-')
                .replaceAll("[àáâãäå]", "a")
                .replaceAll("[èéêë]", "e")
                .replaceAll("[ìíîï]", "i")
                .replaceAll("[òóôõö]", "o")
                .replaceAll("[š]", "s")
                .replaceAll("[ž]", "z")
                .replaceAll("[ùúûü]", "u");
        return String.format(ExternalServicesUrls.PHOTOS_BASE_URL, formattedName, id);
    }

    public URL buildUrl(final String urlString) throws MalformedURLException {
        return new URL(urlString);
    }
}
