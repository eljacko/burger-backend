package com.qminder.burgerbackend.util;

import io.undertow.server.handlers.accesslog.AccessLogReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BackendAccessLogReceiver implements AccessLogReceiver {
    public static final Logger ACCESS_LOG_LOGGER = LoggerFactory.getLogger("access_log");

    public final void logMessage(final String message) {
        if (ACCESS_LOG_LOGGER.isInfoEnabled()) {
            ACCESS_LOG_LOGGER.info(message);
        }
    }
}
