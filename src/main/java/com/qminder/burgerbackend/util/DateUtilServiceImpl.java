package com.qminder.burgerbackend.util;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;

@Service
@AllArgsConstructor
public class DateUtilServiceImpl implements DateUtilService {
    @Override
    public final java.util.Date getDate() {
        return new java.util.Date();
    }

    @Override
    public final Timestamp getCurrentTimeAsTimestamp() {
        LocalDateTime today = LocalDateTime.now();
        return Timestamp.valueOf(today);
    }

    @Override
    public final Instant getInstantNow() {
        return Instant.now();
    }

    @Override
    public final String getDateAsVersion() {
        return new SimpleDateFormat("yyyyMMdd").format(new Date());
    }
}
