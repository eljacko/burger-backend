package com.qminder.burgerbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
public class BurgerBackendApplication {

	public BurgerBackendApplication() {
		super();
	}

	public static void main(final String[] args) {
		SpringApplication.run(BurgerBackendApplication.class, args);
	}

}
