## Building

To build everything:

./gradlew build

To execute task:

./gradlew bootRun

To build all spring-boot jars:

./gradlew clean bootJar